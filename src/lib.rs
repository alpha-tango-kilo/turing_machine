#![deny(missing_docs)]

//! # A Turing machine library
//!
//! You create a Turing machine using either [`SimpleTuringMachine`] or [`AdvancedTuringMachine`].
//! Alternatively, you can build one yourself by implementing [`TuringMachine`].
//!
//! Before using either of the pre-fabricated Turing machines, you will want to have defined a type to use for the Turing machine's state, usually an enum.

use std::cell::RefCell;
pub use std::collections::{HashMap, HashSet};
use std::fmt::{self, Debug};
use std::hash::Hash;

/// A map from the current state of the turing machine to the transition function to perform.
///
/// The key is written as a tuple of the turing machine state and current cell value.
/// The value is the [`TransitionFunction`] to be run when in this state
pub type TransitionMap<S, T> = HashMap<(S, T), TransitionFunction<S, T>>;

/// A function that modifies a [Turing machine's internal state](TMState)
///
/// # Example
///
/// ```
/// # use turing_machine::*;
/// # #[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
/// enum State { A, B }
/// let _tf: TransitionFunction<State, i32> = |tm| {
///     tm.current_state = State::B;
///     tm.left(3);
/// };
///
/// // or
///
/// fn left_one(tm: &mut TMState<State, i32>) {
///     tm.left(1);
/// }
/// ```
pub type TransitionFunction<S, T> = fn(&mut TMState<S, T>);

/// Defines the core functionality of a Turing Machine, so that you can create you own under the same generic structure if you wish
pub trait TuringMachine<T> {
    /// Runs one transition function based on the current state
    fn step(&mut self);
    /// Runs until completion and returns the value of the final cell. Will panic if [`step`](TuringMachine::step) panics
    fn run(&mut self) -> T;
    /// Returns true if the Turing machine is in a final state
    fn halted(&self) -> bool;
}

/// The internal state of the Turing machine.
/// This is the part of the Turing you have access to in a [transition function](TransitionFunction)
///
/// Always contained within a [`SimpleTuringMachine`] or [`AdvancedTuringMachine`]
#[derive(Eq, PartialEq)]
pub struct TMState<S, T> {
    /// The current state of the Turing machine
    pub current_state: S,
    pointer: usize,
    tape: Vec<T>,
}

impl<S, T> TMState<S, T>
where
    S: Copy + Eq + Hash,
    T: Copy,
{
    /// Gets a mutable reference to the current cell
    pub fn current(&mut self) -> &mut T {
        &mut self.tape[self.pointer]
    }

    /// Returns the index of the pointer
    pub fn pointer(&mut self) -> usize {
        self.pointer
    }

    /// Moves the pointer left by `cells` number of cells
    ///
    /// Wraps if it reaches the end of the tape
    pub fn left(&mut self, cells: usize) {
        self.pointer = self
            .pointer
            .checked_sub(cells)
            .unwrap_or_else(|| self.tape.len() - (self.pointer - cells));
    }

    /// Moves the pointer right by `cells` number of cells
    ///
    /// Wraps if it reaches the end of the tape
    pub fn right(&mut self, cells: usize) {
        self.pointer = (self.pointer + cells) % self.tape.len();
    }

    fn as_key(&self) -> (S, T) {
        (self.current_state, self.tape[self.pointer])
    }
}

impl<S: Debug, T: Debug> Debug for TMState<S, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({:?}, {:?})", self.current_state, self.pointer)
    }
}

/// An easier to use Turing machine
///
/// Easier to use at the cost of tedium if you intend to solve any sort of problem
///
/// `S`: State (enum)
///
/// `T`: Tape type (number)
///
/// `S` & `T` should both be `Copy + Debug + Eq + Hash`
/// 
/// # Example
///
/// ```
/// use turing_machine::*;
///
/// #[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
/// enum State { A, B }
///
/// # #[allow(clippy::needless_doctest_main)]
/// fn main() {
///     // start state
///     let start = State::A;
///     // the tape
///     let tape = vec![1, 2, 3, 4, 5];
///     // a transition function (you probably want more than one ordinarily)
///     let tf = |tm: &mut TMState<State, i32>| {
///         tm.current_state = State::B;
///         tm.right(3);
///     };
///     // type annotation needed here to avoid type mismatch
///     let mut transition_functions: TransitionMap<State, i32> = HashMap::new();
///     // transition function corresponds to state A, cell value 1
///     transition_functions.insert((State::A, 1), tf);
///     let mut final_states = HashSet::new();
///     // make B a final state
///     final_states.insert(State::B);
///
///     let mut tm = SimpleTuringMachine::new(start, tape, transition_functions, final_states);
///     let final_result = tm.run(); // completes in one step
///     assert_eq!(final_result, 4);
/// }
/// ```
pub struct SimpleTuringMachine<S, T> {
    state: RefCell<TMState<S, T>>,
    transitions: TransitionMap<S, T>,
    final_states: HashSet<S>,
}

impl<S, T> SimpleTuringMachine<S, T>
where
    S: Copy + Debug + Eq + Hash,
    T: Copy + Debug + Eq + Hash,
{
    /// Creates a new Turing machine
    pub fn new(
        start: S,
        tape: Vec<T>,
        transitions: TransitionMap<S, T>,
        final_states: HashSet<S>,
    ) -> Self {
        SimpleTuringMachine {
            state: RefCell::new(TMState {
                current_state: start,
                pointer: 0,
                tape,
            }),
            transitions,
            final_states,
        }
    }
}

impl<S, T> TuringMachine<T> for SimpleTuringMachine<S, T>
where
    S: Copy + Debug + Eq + Hash,
    T: Copy + Debug + Eq + Hash,
{
    /// Simulates one step of the turing machine
    ///
    /// So long as the machine isn't in a final state, this will:
    /// 1. Read the current tape location
    /// 2. Look up the transition function to use
    /// 3. Apply the transition function
    ///
    /// # Panics
    ///
    /// This function will panic if there is no [`TransitionFunction`] in [`TransitionMap`] that
    /// applies to the current state
    fn step(&mut self) {
        if !self.halted() {
            let state = self.state.borrow().as_key();
            let func = self
                .transitions
                .get(&state)
                .unwrap_or_else(|| panic!("No transition function found for state {:?}", state));
            func(&mut self.state.borrow_mut());
        }
    }

    fn run(&mut self) -> T {
        while !self.halted() { self.step() }
        let state = self.state.borrow();
        state.tape[state.pointer]
    }

    fn halted(&self) -> bool {
        let state = self.state.borrow();
        self.final_states.contains(&state.current_state)
    }
}

impl<S: Debug, T: Debug> Debug for SimpleTuringMachine<S, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.state)
    }
}

/// Requires that the type is able to produce an index, with which to find a [`TransitionFunction`].
///
/// In an [`AdvancedTuringMachine`], a map is created between the current state and transition
/// predicates. A predicate is called with the value of the current cell to then provide an index
/// into the list of [`TransitionFunction`]s an [`AdvancedTuringMachine`], which is the transition
/// function which will then be called.
pub trait TransitionPredicate<T> {
    /// Takes the current cell value and produces an index
    fn index(&self, current_cell: T) -> usize;
}

impl<T> TransitionPredicate<T> for usize {
    fn index(&self, _current_cell: T) -> usize {
        *self
    }
}

impl<T> TransitionPredicate<T> for fn() -> usize {
    fn index(&self, _current_cell: T) -> usize {
        self()
    }
}

impl<T> TransitionPredicate<T> for fn(T) -> usize {
    fn index(&self, current_cell: T) -> usize {
        self(current_cell)
    }
}

/// A bigger, better, uglier Turing machine
///
/// `S`: State (enum)
///
/// `T`: Tape type (number)
///
/// Instead of having to account for every combination of `S` and `T` to make a [`TransitionMap`],
/// the advanced turing machine instead uses [a function](TransitionPredicate) that's determined
/// by the current state and the value of the cell to choose which transition function to use.
/// This is intended to save a lot of duplicate closures/functions needing to be given, and
/// effectively allows the library user to `match` over `T` to decide what to do
pub struct AdvancedTuringMachine<S, T> {
    state: RefCell<TMState<S, T>>,
    predicates: HashMap<S, Box<dyn TransitionPredicate<S>>>,
    transitions: Vec<TransitionFunction<S, T>>,
    final_states: HashSet<S>,
}

impl<S, T> AdvancedTuringMachine<S, T>
where
    S: Copy + Debug + Eq + Hash,
    T: Copy + Debug,
{
    /// Creates a new Turing machine
    pub fn new(
        start: S,
        tape: Vec<T>,
        predicates: HashMap<S, Box<dyn TransitionPredicate<S>>>,
        transitions: Vec<TransitionFunction<S, T>>,
        final_states: HashSet<S>,
    ) -> Self {
        AdvancedTuringMachine {
            state: RefCell::new(TMState {
                current_state: start,
                pointer: 0,
                tape,
            }),
            predicates,
            transitions,
            final_states,
        }
    }
}

impl<S, T> TuringMachine<T> for AdvancedTuringMachine<S, T>
where
    S: Copy + Debug + Eq + Hash,
    T: Copy + Debug,
{
    /// Simulates one step of the turing machine
    ///
    /// So long as the machine isn't in a final state, this will:
    /// 1. Choose a [`TransitionPredicate`] based on the current state
    /// 2. Evaluate the transition predicate [`index`](TransitionPredicate::index)
    /// 3. Apply the transition function from the index
    ///
    /// # Panics
    ///
    /// * If there is no [`TransitionPredicate`] for the current state (and the state isn't a final state)
    /// * If the [index produced](TransitionPredicate::index) is out of range of the list of transition functions
    ///
    fn step(&mut self) {
        if !self.halted() {
            let current_state = self.state.borrow().current_state;
            let pred = self
                .predicates
                .get(&current_state)
                .unwrap_or_else(|| panic!("No transition predicate for {:?}", current_state));
            let tf_index = pred.index(current_state);
            let tf = self.transitions.get(tf_index)
                .unwrap_or_else(|| panic!("Transition predicate for {:?} gave an out-of-range index for transition functions", current_state));
            tf(&mut self.state.borrow_mut())
        }
    }

    fn run(&mut self) -> T {
        while !self.halted() { self.step() }
        let state = self.state.borrow();
        state.tape[state.pointer]
    }

    fn halted(&self) -> bool {
        let state = self.state.borrow();
        self.final_states.contains(&state.current_state)
    }
}

impl<S: Debug, T: Debug> Debug for AdvancedTuringMachine<S, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.state)
    }
}

/// An unrealistic, but usable Turing machine
///
/// Tired of deciding what to do before you do it? This Turing machine's the one for you. There is
/// only one transition function, which gives you the freedom to make the Turing machine operate
/// easily, without having to pick which transition function to call.
pub struct UsableTuringMachine<S, T> {
    state: RefCell<TMState<S, T>>,
    transition: fn(&mut TMState<S, T>),
    final_states: HashSet<S>,
}

impl<S, T> UsableTuringMachine<S, T>
where
    S: Copy + Eq + Hash,
    T: Copy,
{
    /// Creates a usable Turing machine
    pub fn new(
        start: S,
        tape: Vec<T>,
        transition: fn(&mut TMState<S, T>),
        final_states: HashSet<S>
    ) -> Self {
        UsableTuringMachine {
            state: RefCell::new(TMState {
                current_state: start,
                pointer: 0,
                tape,
            }),
            transition,
            final_states,
        }
    }
}

impl<S, T> TuringMachine<T> for UsableTuringMachine<S, T>
where
    S: Copy + Eq + Hash,
    T: Copy,
{
    /// Runs the transition function if not in a final state
    fn step(&mut self) {
        if !self.halted() {
            (self.transition)(&mut self.state.borrow_mut());
        }
    }

    fn run(&mut self) -> T {
        while !self.halted() { self.step() }
        let state = self.state.borrow();
        state.tape[state.pointer]
    }

    fn halted(&self) -> bool {
        let current_state = self.state.borrow().current_state;
        self.final_states.contains(&current_state)
    }
}
