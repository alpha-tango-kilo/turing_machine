# Turing machine

Turing machines, in Rust!

```rust
use turing_machine::*;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
enum State { A, B }

fn main() {
    // start state
    let start = State::A;
    // the tape
    let tape = vec![1, 2, 3, 4, 5];
    // a transition function (you probably want more than one ordinarily)
    let tf = |tm: &mut TMState<State, i32>| {
        tm.current_state = State::B;
        tm.right(3);
    };
    // type annotation needed here to avoid type mismatch
    let mut transition_functions: Transitions<State, i32> = HashMap::new();
    // transition function corresponds to state A, cell value 1
    transition_functions.insert((State::A, 1), tf);
    let mut final_states = HashSet::new();
    // make B a final state
    final_states.insert(State::B);

    let mut tm = SimpleTuringMachine::new(start, tape, transition_functions, final_states);
    let final_result = tm.run(); // completes in one step
    assert_eq!(final_result, 4);
}
```
